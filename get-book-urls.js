;(() => {
  const rows = [...document.getElementsByClassName('adbl-library-content-row')]
  console.log(`Found ${rows.length} rows`)
  const books = rows.reduce((accum, curr) => {
    const author = curr.querySelector('.authorLabel .bc-link').textContent.trim()
    const title = curr.querySelector('.bc-size-headline3').textContent.trim()
    const downloadUrl = (() => {
      const anchors = [...curr.querySelectorAll('a')]
      const matches = anchors.filter(e => e.href && e.parentElement.id.startsWith('download-button'))
      if (matches.length > 1) {
        console.warn(`WARN: "${title}" found more than one download URL, probably a bug in this script`)
      }
      if (matches.length === 0) {
        // seems to happen for the top-level podcast container, but I've seen
        // at least one book that just didn't have a download link
        console.warn(`WARN: "${title}" has no download URL`)
        return '(no URL)'
      }
      return matches[0].href
    })()
    accum.push({
      author,
      title,
      full: `${title} -- ${author}`,
      downloadUrl,
    })
    return accum
  }, [])
  const booklistStr = books.map(b => `${b.downloadUrl};${b.full}`).join('<br />')
  const theHtmlStr = `
    <dialog open style="z-index: 99999; position: fixed;">
      <p style="overflow-y: scroll; max-height: 90vh; max-width: 80vw;">${booklistStr}</p>
      <form method="dialog">
        <button>OK</button>
      </form>
    </dialog>`
  const body = document.getElementsByTagName('body')[0]
  body.insertAdjacentHTML('afterbegin', theHtmlStr)
})()
