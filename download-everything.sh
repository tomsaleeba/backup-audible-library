#!/usr/bin/env bash
set -euo pipefail
cd "$(dirname "$0")"

outDir=output
mkdir -p $outDir
cookiesFile=./cookies.txt
if [ ! -f "$cookiesFile" ]; then
  echo "[ERROR] cookies file ${cookiesFile} doesn't exist!"
  echo "  Create it as 'abc=123; def=456;' and retry this command"
  exit 1
fi
# I don't know which ones are needed, just send them all
cookies=$(cat "$cookiesFile")

while IFS= read -r curr; do
  theUrl=$(echo "$curr" | sed 's/;.*//')
  theTitle=$(echo "$curr" | sed 's/.*;//' | sed 's/[^a-zA-Z0-9.]/_/g')
  outFileName=${outDir}/${theTitle}.aax
  if [ -f "$outFileName" ]; then
    echo "already exists, skipping: ${outFileName}"
    # TODO we could `curl --head ...` to get the Content-Length to confirm the
    # file is fully downloaded. The endpoint works but is sloooow!
    continue
  fi
  echo "downloading: ${outFileName} from ${theUrl}"
  wget \
    --user-agent='User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:109.0) Gecko/20100101 Firefox/109.0' \
    --header="Cookie: ${cookies}" \
    --output-document="$outFileName" \
    "$theUrl"
done < download-urls.txt
