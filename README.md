How to backup your Audible library

## Get all the download URLs for your books
1. Open the web app
1. go to your library
1. set the page size to `50` 
1. for each page
  - run the `./get-book-urls.js` script in the browser console
  - copy the list into a `txt` file
1. once you have all the URLs, remove the rows from your `txt` file that contain `no URL`

The final file should be `./download-urls.txt`


## Download all the `aax` files using the URLs
1. get your cookies from the Audible webapp by
  - open browser devtools
  - change page
  - look for the `GET titles?...` request
  - "copy as curl" that request
  - grab just the value of the *request* cookie header, e.g. `ab=12; cd=34;`
  - stick that into `./cookies.txt`
1. run the `download-everything.sh` script

Note: the script is idempotent so it will skip existing files.
It won't check if the file was completely downloaded though.


## Get your activation_bytes
The `.aax` files are encrypted, and you need your personal "key" to decrypt
them. The key is unqiue to you, not per-book, so you only need to get it once.

Options for how to get it, that pull the data from Audible servers:
- https://github.com/mkb79/audible-cli 
- https://github.com/inAudible-NG/audible-activator

Or, if you'd rather not enter your credentials into some random tool, you can
compute the activation bytes offline with:
- https://github.com/inAudible-NG/tables (a copy of this repo is in `./inaudible-ng_tables.tar.gz`)


## Decrypt and convert the aax files
You can get a batch converter that runs in a docker container from:
[https://github.com/tomsaleeba/AAXtoMP3](). Clone that repo and run:
```
./batch-convert-with-docker.sh a5c68103 ~/path/to/aax-files/
```


## How to listen to your audiobooks
They're just mp3s, so you can use pretty much any media player. For a more
audiobook focused experience, [https://www.audiobookshelf.org/]() is pretty
great.
